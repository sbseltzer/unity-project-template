$ASSETS_PATH = (Get-ChildItem "../" -Recurse -filter "Assets" -Directory -Name | Select-Object -First 1)
$UNITY_PROJECT = (get-item "..\$ASSETS_PATH" ).parent.Name
$ASSETS_PATH = "$UNITY_PROJECT\Assets"
$env:UNITY_PROJECT = $UNITY_PROJECT
$env:ASSETS_PATH = $ASSETS_PATH
echo $UNITY_PROJECT
echo $ASSETS_PATH
doxygen Doxyfile